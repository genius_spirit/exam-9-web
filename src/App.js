import React, { Component, Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import PhoneBook from "./containers/PhoneBook/PhoneBook";
import AddContact from "./containers/AddContact/AddContact";
import Nav from "./components/Nav/Nav";
import EditContact from "./containers/EditContact/EditContact";

class App extends Component {
  render() {
    const textStyle = {
      textAlign: 'center',
      fontSize: '30px'
    };
    return (
      <Fragment>
        <Nav/>
        <div style={{margin: "0 auto", textAlign: "center", maxWidth: "1140px"}}>
          <Switch>
            <Route exact path="/" component={PhoneBook} />
            <Route exact path="/contact/add" component={AddContact} />
            <Route exact path="/contact/edit" component={EditContact} />
            <Route render={() => <h1 style={textStyle}>Page not found</h1>} />
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default App;
