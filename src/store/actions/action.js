import axios from 'axios';
const URL = 'https://blog-187e7.firebaseio.com';

export const START_LOADER = 'START_LOADER';
export const STOP_LOADER = 'STOP_LOADER';

export const ORDER_SUCCESS = "ORDER_SUCCESS";
export const ORDER_FAILURE = "ORDER_FAILURE";

export const GET_ID = "GET_ID";

export const startLoader = () => {
  return {type: START_LOADER};
};

export const stopLoader = () => {
  return {type: STOP_LOADER};
};

export const orderSuccess = (contacts) => {
  return {type: ORDER_SUCCESS, contacts};
};

export const  orderFailure = (error) => {
  return {type: ORDER_FAILURE, error};
};

export const addNewContact = (contact) => dispatch => {
  dispatch(startLoader());
  axios.post(`${URL}/contacts.json`, contact).then(() => dispatch(stopLoader()));
};

export const getContacts = () => dispatch => {
  dispatch(startLoader());
  axios.get(`${URL}/contacts.json`).then(response => {
    dispatch(orderSuccess(response.data));
    dispatch(stopLoader());
  }, error => {
    dispatch(orderFailure(error));
  })
};

export const getId = (id) => {
  return {type: GET_ID, id};
};

export const removeContact = (id) => dispatch => {
  axios.delete(`${URL}/contacts/${id}.json`);
};

export const editContact = (newContact, id) => dispatch => {
  dispatch(startLoader());
  axios.patch(`${URL}/contacts/${id}.json`, newContact).then(() => {
    dispatch(stopLoader());
  })
};