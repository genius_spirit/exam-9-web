import React, { Component } from 'react';
import {connect} from "react-redux";
import {Button} from "material-ui";
import './AddContact.css';
import {Link} from "react-router-dom";
import {addNewContact} from "../../store/actions/action";

class AddContact extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    photo: ''
  };

  valueChanged = (e) => {
    const name = e.target.name;
    this.setState({[name]: e.target.value});
  };

  addContactHandler = (event) => {
    event.preventDefault();
    const contact = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      photo: this.state.photo
    };
    this.props.addNewContact(contact);
    this.setState({name: '', email: '', phone: '', photo: ''})
  };

  render() {
    return (
      <div className="ContactData">
        <h2>Add new contact</h2>
        <form>
          <div className="photo-img">
            <span>Photo preview</span>
            <div><img src={this.state.photo ? this.state.photo : null} alt="contact" width="150" height="100"/></div>
          </div>
          <input className="Input" type="text" name="name" placeholder="Your Name"
                            value={this.state.name} onChange={this.valueChanged}/>
          <input className="Input" type="number" name="phone" placeholder="Your Phone"
                 value={this.state.phone} onChange={this.valueChanged}/>
          <input className="Input" type="email" name="email" placeholder="Your Mail"
                 value={this.state.email} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="photo" placeholder="Your Photo"
                 value={this.state.photo} onChange={this.valueChanged}/>

          <Button variant="raised" size="small" className="btn" style={{margin: '5px 10px'}}
                  color="primary" onClick={this.addContactHandler}>Save</Button>
          <Button variant="raised" size="small" style={{margin: '5px 10px'}}
                  component={Link} to="/">Back to contacts</Button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addNewContact: (contact) => dispatch(addNewContact(contact))
  };
};

export default connect(null, mapDispatchToProps)(AddContact);