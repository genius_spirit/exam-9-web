import { GET_ID, ORDER_FAILURE, ORDER_SUCCESS, START_LOADER, STOP_LOADER} from "../actions/action";

const initialState = {
  loading:  false,
  contacts: [],
  id: '',
  showModal: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ID:
      return {...state, id: action.id};
    case START_LOADER:
      return {...state, loading: true};
    case STOP_LOADER:
      return {...state, loading: false};
    case ORDER_SUCCESS:
      return {...state, contacts: action.contacts};
    case ORDER_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;